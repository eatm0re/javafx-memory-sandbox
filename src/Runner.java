import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Scanner;

public class Runner {

  public static void main(String[] args) {
    Queue<HeavyObject> queue = new ArrayDeque<>();
    Scanner in = new Scanner(System.in);
    String command;
    while (!(command = in.nextLine()).equalsIgnoreCase("exit")) {
      switch (command.toLowerCase()) {
        case "create":
          queue.add(new HeavyObject());
          break;
        case "remove":
          queue.poll();
          break;
        default:
      }
      System.out.printf("Now in memory %d elements%n", queue.size());
    }
  }
}
