import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class RunnerFX extends Application {

  private HeavyObjectContainer container = new HeavyObjectContainer();

  @Override
  public void start(Stage primaryStage) throws Exception {

    Button createButton = new Button();
    createButton.setText("Create");
    createButton.setOnAction(event -> container.create());

    Button removeButton = new Button();
    removeButton.setText("Remove");
    removeButton.setOnAction(event -> container.remove());

    HBox root = new HBox();
    root.getChildren().add(createButton);
    root.getChildren().add(removeButton);
    Scene scene = new Scene(root, 300, 200);
    primaryStage.setTitle("Java Memory Sandbox");
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }
}
