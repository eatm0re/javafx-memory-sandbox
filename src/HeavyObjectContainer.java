import java.util.ArrayDeque;
import java.util.Queue;

public class HeavyObjectContainer {

  private Queue<HeavyObject> storage = new ArrayDeque<>();

  public void create() {
    storage.add(new HeavyObject());
    reportSize();
  }

  public void remove() {
    storage.poll();
    reportSize();
  }

  private void reportSize() {
    System.out.printf("There are %d elements in the storage now%n", storage.size());
  }
}
